package com.example.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.example.demo.model.Employee;
import com.example.demo.repositories.EmployeeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@RestController
@SpringBootApplication
public class DemoApplication {
	
	@Autowired
	EmployeeRepository employeeRepository;
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@GetMapping("/helloworld")
	public String helloworld(){
		return "Hello World";
	}
	@GetMapping("/bmi")
	public Double bmi (@RequestParam(value="height") Double height , @RequestParam(value="weight") Double weight ){	
		System.out.println("height :" +height);
		System.out.println("weight :" +weight);
		return 	weight / ((height/100) * (height/100));
	}


	//CRUD
	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping(value = "/addEmployee" , consumes = "application/json" , produces =  "application/json")
	public ResponseEntity<String> addEmployee (@RequestBody Employee employee){	
	
		employee.setId(UUID.randomUUID().toString());	
		employee.setEmployeeId(UUID.randomUUID().toString());

		employeeRepository.save(employee);
		return 	new ResponseEntity<String>( "Succes" , HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping(value = "/updateEmployee" , consumes = "application/json" , produces =  "application/json")
	public ResponseEntity<String>  updateEmployee (@RequestBody Employee employee){	
	
		employeeRepository.save(employee);
		return 	new ResponseEntity<String>( "Succes" , HttpStatus.OK);
	}


	@CrossOrigin(origins = "http://localhost:3000")
	@DeleteMapping(value = "/deleteEmployee/{id}")
	public ResponseEntity<String>  deleteEmployee (@PathVariable String id){	
	
		employeeRepository.deleteById(id);

		return 	new ResponseEntity<String>( "Succes" , HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping(value = "/getEmployee/{id}")
	public ResponseEntity<Employee>  getEmployee (@PathVariable String id){	
	
		Employee result = new Employee();
		List<Employee>  response = employeeRepository.findAll();
		for (Employee employee : response) {
			if(employee.getId().equals(id)){
				result = employee;
			}
			
		}
		return 	new ResponseEntity<Employee>( result , HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping(value = "/getAllEmployees")
	public ResponseEntity<List<Employee>>  getEmployees (){	
		
		List<Employee> response = new ArrayList<Employee>();
		response = employeeRepository.findAll();

		return 	new ResponseEntity<List<Employee>>( response , HttpStatus.OK);
	}



}
